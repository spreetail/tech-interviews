# Nightwatch Skeleton

#### How to use

Requirements:

* OSX or *nix
* latest chrome
* latest java

Clone the tech interviews repo on to your machine

    git clone git@bitbucket.org:spreetail/tech-interviews.git

Change to the nightwatch-skeleton directory:

     cd nightwatch-skeleton/

install the modules:

    npm i

then run the tests:

    npm test
