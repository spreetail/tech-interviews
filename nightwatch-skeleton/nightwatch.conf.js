module.exports = {
  src_folders: ['tests'],
  custom_commands_path: 'tests/commands',
  page_objects_path: 'tests/pages',
  output_folder: 'reports',
  webdriver: {
    start_process: true,
  },

  test_settings: {
    default: {
      launch_url: "https://www.spreetail.com",
      desiredCapabilities: {
        browserName: 'chrome',
        javascriptEnabled: true,
        acceptSslCerts: true,
        databaseEnabled: true,
        applicationCacheEnabled: true,
        webStorageEnabled: true,
        loggingPrefs: { driver: 'INFO', server: 'OFF', browser: 'INFO' },
        chromeOptions: {
          prefs: {
            intl: {
              accept_languages: 'en-US',
            },
            credentials_enable_service: false,
            'profile.password_manager_enabled': false,
          },
        },
        args: [
          '--window-size=1920,1280',
          'ignore-certificate-errors',
          'disable-web-security',
        ],
      },
      webdriver: {
        port: 5555,
        server_path:
          './node_modules/selenium-standalone/.selenium/chromedriver/2.43-x64-chromedriver',
        cli_args: [
          '--verbose',
          '--log-path=reports/chromedriver.log',
          '--port=5555',
        ],
      },
    },

    test_runner: {
      type: 'mocha',
      options: {
        ui: 'bdd',
        reporter: 'list',
      },
    },

    selenium_server: {
      selenium: {
        start_process: true,
        host: 'localhost',
        server_path:
          './node_modules/selenium-standalone/.selenium/selenium-server/3.141.5-server.jar',
        cli_args: {
          'webdriver.gecko.driver': './bin/geckodriver-0.23',
          'webdriver.chrome.driver': './bin/chromedriver-2.32',
        },
      },
    },
  },
};
